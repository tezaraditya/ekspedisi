<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shipping_price".
 *
 * @property int $id_shipping_price
 * @property string $hometown
 * @property string $destination
 * @property string $service_type
 * @property string $shipping_price
 *
 * @property City $hometown0
 * @property City $destination0
 * @property ServiceType $serviceType
 */
class ShippingPrice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shipping_price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hometown', 'destination', 'service_type', 'shipping_price'], 'required'],
            [['hometown', 'destination', 'service_type'], 'string', 'max' => 100],
            [['shipping_price'], 'string', 'max' => 20],
            [['hometown'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['hometown' => 'city']],
            [['destination'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['destination' => 'city']],
            [['service_type'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceType::className(), 'targetAttribute' => ['service_type' => 'service_type']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_shipping_price' => 'Id Shipping Price',
            'hometown' => 'Hometown',
            'destination' => 'Destination',
            'service_type' => 'Service Type',
            'shipping_price' => 'Shipping Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHometown0()
    {
        return $this->hasOne(City::className(), ['city' => 'hometown']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination0()
    {
        return $this->hasOne(City::className(), ['city' => 'destination']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceType()
    {
        return $this->hasOne(ServiceType::className(), ['service_type' => 'service_type']);
    }
}
