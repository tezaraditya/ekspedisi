<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ShippingPrice;

/**
 * ShippingPriceSearch represents the model behind the search form of `app\models\ShippingPrice`.
 */
class ShippingPriceSearch extends ShippingPrice
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_shipping_price'], 'integer'],
            [['hometown', 'destination', 'service_type', 'shipping_price'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShippingPrice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_shipping_price' => $this->id_shipping_price,
        ]);

        $query->andFilterWhere(['like', 'hometown', $this->hometown])
            ->andFilterWhere(['like', 'destination', $this->destination])
            ->andFilterWhere(['like', 'service_type', $this->service_type])
            ->andFilterWhere(['like', 'shipping_price', $this->shipping_price]);

        return $dataProvider;
    }
}
