<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShippingPriceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shipping Prices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipping-price-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Shipping Price', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_shipping_price',
            'hometown',
            'destination',
            'service_type',
            'shipping_price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
