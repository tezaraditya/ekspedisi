<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id_city
 * @property string $province
 * @property string $city
 * @property string $region
 *
 * @property Delivery[] $deliveries
 * @property Delivery[] $deliveries0
 * @property ShippingPrice[] $shippingPrices
 * @property ShippingPrice[] $shippingPrices0
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['province', 'city', 'region'], 'required'],
            [['province', 'city', 'region'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_city' => 'Id City',
            'province' => 'Province',
            'city' => 'City',
            'region' => 'Region',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveries()
    {
        return $this->hasMany(Delivery::className(), ['hometown' => 'city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveries0()
    {
        return $this->hasMany(Delivery::className(), ['destination' => 'city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingPrices()
    {
        return $this->hasMany(ShippingPrice::className(), ['hometown' => 'city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingPrices0()
    {
        return $this->hasMany(ShippingPrice::className(), ['destination' => 'city']);
    }
}
