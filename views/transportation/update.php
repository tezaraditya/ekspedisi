<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Transportation */

$this->title = 'Update Transportation: ' . $model->id_transportation;
$this->params['breadcrumbs'][] = ['label' => 'Transportations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_transportation, 'url' => ['view', 'id' => $model->id_transportation]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="transportation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
