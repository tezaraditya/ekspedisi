<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ShippingPriceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shipping-price-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_shipping_price') ?>

    <?= $form->field($model, 'hometown') ?>

    <?= $form->field($model, 'destination') ?>

    <?= $form->field($model, 'service_type') ?>

    <?= $form->field($model, 'shipping_price') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
