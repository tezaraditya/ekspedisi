<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Transportation */

$this->title = $model->id_transportation;
$this->params['breadcrumbs'][] = ['label' => 'Transportations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transportation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_transportation], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_transportation], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_transportation',
            'transportation',
            'transportation_cost',
        ],
    ]) ?>

</div>
