<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Delivery;

/**
 * DeliverySearch represents the model behind the search form of `app\models\Delivery`.
 */
class DeliverySearch extends Delivery
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_delivery', 'qty', 'item_weight', 'length', 'width', 'height', 'volume_weight'], 'integer'],
            [['sender_name', 'recipient_name', 'hometown', 'destination', 'recipient_address', 'postal_code', 'sender_phone', 'recipient_phone', 'item_type', 'transportation', 'shipping_cost', 'insurance_cost', 'other_cost', 'total_cost', 'receipt_code', 'created_at', 'created_by', 'delivery_status', 'item_receiver', 'service_type'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Delivery::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_delivery' => $this->id_delivery,
            'qty' => $this->qty,
            'item_weight' => $this->item_weight,
            'length' => $this->length,
            'width' => $this->width,
            'height' => $this->height,
            'volume_weight' => $this->volume_weight,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'sender_name', $this->sender_name])
            ->andFilterWhere(['like', 'recipient_name', $this->recipient_name])
            ->andFilterWhere(['like', 'hometown', $this->hometown])
            ->andFilterWhere(['like', 'destination', $this->destination])
            ->andFilterWhere(['like', 'recipient_address', $this->recipient_address])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'sender_phone', $this->sender_phone])
            ->andFilterWhere(['like', 'recipient_phone', $this->recipient_phone])
            ->andFilterWhere(['like', 'item_type', $this->item_type])
            ->andFilterWhere(['like', 'transportation', $this->transportation])
            ->andFilterWhere(['like', 'shipping_cost', $this->shipping_cost])
            ->andFilterWhere(['like', 'insurance_cost', $this->insurance_cost])
            ->andFilterWhere(['like', 'other_cost', $this->other_cost])
            ->andFilterWhere(['like', 'total_cost', $this->total_cost])
            ->andFilterWhere(['like', 'receipt_code', $this->receipt_code])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'delivery_status', $this->delivery_status])
            ->andFilterWhere(['like', 'item_receiver', $this->item_receiver])
            ->andFilterWhere(['like', 'service_type', $this->service_type]);

        return $dataProvider;
    }
}
