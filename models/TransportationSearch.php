<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transportation;

/**
 * TransportationSearch represents the model behind the search form of `app\models\Transportation`.
 */
class TransportationSearch extends Transportation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_transportation'], 'integer'],
            [['transportation', 'transportation_cost'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transportation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_transportation' => $this->id_transportation,
        ]);

        $query->andFilterWhere(['like', 'transportation', $this->transportation])
            ->andFilterWhere(['like', 'transportation_cost', $this->transportation_cost]);

        return $dataProvider;
    }
}
