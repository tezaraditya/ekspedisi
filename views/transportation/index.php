<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransportationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transportations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transportation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Transportation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_transportation',
            'transportation',
            'transportation_cost',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
