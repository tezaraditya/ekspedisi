<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Delivery */

$this->title = $model->id_delivery;
$this->params['breadcrumbs'][] = ['label' => 'Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_delivery], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_delivery], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_delivery',
            'sender_name',
            'recipient_name',
            'hometown',
            'destination',
            'recipient_address:ntext',
            'postal_code',
            'sender_phone',
            'recipient_phone',
            'item_type',
            'qty',
            'item_weight',
            'length',
            'width',
            'height',
            'volume_weight',
            'transportation',
            'shipping_cost',
            'insurance_cost',
            'other_cost',
            'total_cost',
            'receipt_code',
            'created_at',
            'created_by',
            'delivery_status',
            'item_receiver',
            'service_type',
        ],
    ]) ?>

</div>
