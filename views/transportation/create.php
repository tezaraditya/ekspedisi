<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Transportation */

$this->title = 'Create Transportation';
$this->params['breadcrumbs'][] = ['label' => 'Transportations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transportation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
