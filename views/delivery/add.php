<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Delivery */

$this->title = 'Add Delivery';
$this->params['breadcrumbs'][] = ['label' => 'Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-create">

    <h1><?= Html::encode($this->title) ?></h1>

  <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-6">
	<?= $form->field($model, 'sender_name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'recipient_name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'hometown')->dropDownList(ArrayHelper::map(\app\models\City::find()->asArray()->all(), 'city', 'city'), ['prompt' => '-- Select Hometown --']) ?>

	<?= $form->field($model, 'destination')->dropDownList(ArrayHelper::map(\app\models\City::find()->asArray()->all(), 'city', 'city'), ['prompt' => '-- Select Destination --']) ?>

    <?= $form->field($model, 'recipient_address')->textarea(['rows' => 1]) ?>

    <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sender_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'recipient_phone')->textInput(['maxlength' => true]) ?>

    
	<?= $form->field($model, 'item_type')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'qty')->textInput(['type'=>'number']) ?>

    <?= $form->field($model, 'item_weight')->textInput(['type'=>'number']) ?>

     <?= $form->field($model, 'length')->textInput(['type'=>'number']) ?>

    <div class="col-md-6">
	<?= $form->field($model, 'width')->textInput(['type'=>'number']) ?>

    <?= $form->field($model, 'height')->textInput(['type'=>'number']) ?>

    <?= $form->field($model, 'volume_weight')->textInput(['type'=>'number']) ?>

    <?= $form->field($model, 'transportation')->dropDownList(ArrayHelper::map(\app\models\Transportation::find()->asArray()->all(), 'transportation', 'transportation'), ['prompt' => '-- Select Transportation --']) ?>

    <?= $form->field($model, 'shipping_cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'insurance_cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'other_cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'receipt_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'delivery_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'item_receiver')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'service_type')->dropDownList(ArrayHelper::map(\app\models\ServiceType::find()->asArray()->all(), 'service_type', 'service_type'), ['prompt' => '-- Select Service --']) ?>
	
	</div>

    <div class="col-md-12"><div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-warning btn-lg']) ?>
    </div></div>

    <?php ActiveForm::end(); ?>

</div>
