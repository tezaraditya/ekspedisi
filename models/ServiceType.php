<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "service_type".
 *
 * @property int $id_service_type
 * @property string $service_type
 *
 * @property Delivery[] $deliveries
 * @property ShippingPrice[] $shippingPrices
 */
class ServiceType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_type'], 'required'],
            [['service_type'], 'string', 'max' => 100],
            [['service_type'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_service_type' => 'Id Service Type',
            'service_type' => 'Service Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveries()
    {
        return $this->hasMany(Delivery::className(), ['service_type' => 'service_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingPrices()
    {
        return $this->hasMany(ShippingPrice::className(), ['service_type' => 'service_type']);
    }
}
