<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DeliverySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deliveries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Delivery', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_delivery',
            'sender_name',
            'recipient_name',
            'hometown',
            'destination',
            //'recipient_address:ntext',
            //'postal_code',
            //'sender_phone',
            //'recipient_phone',
            //'item_type',
            //'qty',
            //'item_weight',
            //'length',
            //'width',
            //'height',
            //'volume_weight',
            //'transportation',
            //'shipping_cost',
            //'insurance_cost',
            //'other_cost',
            //'total_cost',
            //'receipt_code',
            //'created_at',
            //'created_by',
            //'delivery_status',
            //'item_receiver',
            //'service_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
