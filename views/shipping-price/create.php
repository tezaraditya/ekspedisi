<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ShippingPrice */

$this->title = 'Create Shipping Price';
$this->params['breadcrumbs'][] = ['label' => 'Shipping Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipping-price-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
