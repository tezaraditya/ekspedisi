<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Login';
?>
<div class="col-lg-4"></div>
<div class="col-lg-4">
<div class="panel panel-default ">
  <div class="panel-heading" >
  <center>
<h3 style="width:100px; height:100px; background:url('<?= Yii::$app->request->baseUrl ?>/public/img/login.png') no-repeat; background-size:cover; background-position:top center;')"></h3>
  </center>
  </div>
  <div class="panel-body">





    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
    ]); ?>



       <div class="col-md-12"> <?= $form->field($model, 'email')->textInput(['placeholder'=>'Email'])->label(false) ?> </div>

       <div class="col-md-12"> <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password'])->label(false) ?> </div>

		<div class="col-md-12"> <?= $form->field($model, 'rememberMe')->checkbox() ?> </div>

        <div class=" col-md-13">
		
         <?= Html::submitButton('Login', ['class' => 'btn btn-warning btn-block btn-lg', 'name' => 'login-button']) ?>
        </div>

      

    
        <div class="col-md-12">
        </div>

    <?php ActiveForm::end(); ?>



</div>
</div>
</div>