<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id_user
 * @property string $email
 * @property string $password
 * @property string $fullname
 * @property string $phone
 * @property string $address
 * @property int $role
 * @property string $authKey
 * @property string $accessToken
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password', 'fullname', 'phone', 'address', 'role'], 'required'],
            [['address'], 'string'],
            [['role'], 'integer'],
            [['email', 'fullname'], 'string', 'max' => 100],
            [['password', 'authKey', 'accessToken'], 'string', 'max' => 500],
            [['phone'], 'string', 'max' => 20],
            [['email'], 'unique'],
            [['phone'], 'unique'],
            [['authKey'], 'unique'],
            [['accessToken'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'email' => 'Email',
            'password' => 'Password',
            'fullname' => 'Fullname',
            'phone' => 'Phone',
            'address' => 'Address',
            'role' => 'Role',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
        ];
    }
	
	
	  public function beforeSave($insert) {
        if(isset($this->password)) {
            
            $this->password = bin2hex($this->password);
            $this->authKey = sha1($this->email);
            $this->accessToken = sha1($this->id_user);
            
            
        }
            return parent::beforeSave($insert);
    }
}
