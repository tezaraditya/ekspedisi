<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transportation".
 *
 * @property int $id_transportation
 * @property string $transportation
 * @property string $transportation_cost
 *
 * @property Delivery[] $deliveries
 */
class Transportation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transportation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transportation', 'transportation_cost'], 'required'],
            [['transportation'], 'string', 'max' => 100],
            [['transportation_cost'], 'string', 'max' => 20],
            [['transportation'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_transportation' => 'Id Transportation',
            'transportation' => 'Transportation',
            'transportation_cost' => 'Transportation Cost',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveries()
    {
        return $this->hasMany(Delivery::className(), ['transportation' => 'transportation']);
    }
}
