<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShippingPrice */

$this->title = 'Update Shipping Price: ' . $model->id_shipping_price;
$this->params['breadcrumbs'][] = ['label' => 'Shipping Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_shipping_price, 'url' => ['view', 'id' => $model->id_shipping_price]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="shipping-price-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
