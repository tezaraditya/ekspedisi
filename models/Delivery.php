<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "delivery".
 *
 * @property int $id_delivery
 * @property string $sender_name
 * @property string $recipient_name
 * @property string $hometown
 * @property string $destination
 * @property string $recipient_address
 * @property string $postal_code
 * @property string $sender_phone
 * @property string $recipient_phone
 * @property string $item_type
 * @property int $qty
 * @property int $item_weight
 * @property int $length
 * @property int $width
 * @property int $height
 * @property int $volume_weight
 * @property string $transportation
 * @property string $shipping_cost
 * @property string $insurance_cost
 * @property string $other_cost
 * @property string $total_cost
 * @property string $receipt_code
 * @property string $created_at
 * @property string $created_by
 * @property string $delivery_status
 * @property string $item_receiver
 * @property string $service_type
 *
 * @property Users $createdBy
 * @property City $hometown0
 * @property City $destination0
 * @property Transportation $transportation0
 * @property ServiceType $serviceType
 */
class Delivery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sender_name', 'recipient_name', 'hometown', 'destination', 'recipient_address', 'postal_code', 'sender_phone', 'recipient_phone', 'item_type', 'qty', 'item_weight', 'transportation', 'shipping_cost', 'insurance_cost', 'other_cost', 'total_cost', 'receipt_code', 'delivery_status', 'item_receiver', 'service_type'], 'required'],
            [['recipient_address'], 'string'],
            [['qty', 'item_weight', 'length', 'width', 'height', 'volume_weight'], 'integer'],
            [['created_at'], 'safe'],
            [['sender_name', 'recipient_name', 'hometown', 'destination', 'item_type', 'transportation', 'created_by', 'delivery_status', 'item_receiver', 'service_type'], 'string', 'max' => 100],
            [['postal_code'], 'string', 'max' => 10],
            [['sender_phone', 'recipient_phone', 'shipping_cost', 'insurance_cost', 'other_cost', 'total_cost'], 'string', 'max' => 20],
            [['receipt_code'], 'string', 'max' => 50],
            [['receipt_code'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by' => 'email']],
            [['hometown'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['hometown' => 'city']],
            [['destination'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['destination' => 'city']],
            [['transportation'], 'exist', 'skipOnError' => true, 'targetClass' => Transportation::className(), 'targetAttribute' => ['transportation' => 'transportation']],
            [['service_type'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceType::className(), 'targetAttribute' => ['service_type' => 'service_type']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_delivery' => 'Id Delivery',
            'sender_name' => 'Sender Name',
            'recipient_name' => 'Recipient Name',
            'hometown' => 'Hometown',
            'destination' => 'Destination',
            'recipient_address' => 'Recipient Address',
            'postal_code' => 'Postal Code',
            'sender_phone' => 'Sender Phone',
            'recipient_phone' => 'Recipient Phone',
            'item_type' => 'Item Type',
            'qty' => 'Qty',
            'item_weight' => 'Item Weight',
            'length' => 'Length',
            'width' => 'Width',
            'height' => 'Height',
            'volume_weight' => 'Volume Weight',
            'transportation' => 'Transportation',
            'shipping_cost' => 'Shipping Cost',
            'insurance_cost' => 'Insurance Cost',
            'other_cost' => 'Other Cost',
            'total_cost' => 'Total Cost',
            'receipt_code' => 'Receipt Code',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'delivery_status' => 'Delivery Status',
            'item_receiver' => 'Item Receiver',
            'service_type' => 'Service Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['email' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHometown0()
    {
        return $this->hasOne(City::className(), ['city' => 'hometown']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDestination0()
    {
        return $this->hasOne(City::className(), ['city' => 'destination']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransportation0()
    {
        return $this->hasOne(Transportation::className(), ['transportation' => 'transportation']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceType()
    {
        return $this->hasOne(ServiceType::className(), ['service_type' => 'service_type']);
    }
	
	
	public function beforeSave($insert) {
		
		$this->created_at = ('Y-m-d H:i:s');
		$this->created_by = Yii::$app->user->identity->email;
		return parent::beforeSave($insert);
		
		
		
	}
}
