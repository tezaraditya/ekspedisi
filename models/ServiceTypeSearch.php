<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ServiceType;

/**
 * ServiceTypeSearch represents the model behind the search form of `app\models\ServiceType`.
 */
class ServiceTypeSearch extends ServiceType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_service_type'], 'integer'],
            [['service_type'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ServiceType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_service_type' => $this->id_service_type,
        ]);

        $query->andFilterWhere(['like', 'service_type', $this->service_type]);

        return $dataProvider;
    }
}
